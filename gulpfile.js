'use strict';

var gulp = require('gulp');
var browserify = require('browserify');
var source = require('vinyl-source-stream');
var buffer = require('vinyl-buffer');
var uglify = require('gulp-uglify');

function br(name) {
  browserify(__dirname + '/' + name + '/index.js')
    .bundle()
    .pipe(source(name + '.min.js'))
    .pipe(buffer())
    .pipe(uglify())
    .pipe(gulp.dest(__dirname + '/release'));
}

gulp.task('browserify', function () {
  br('main');
  br('foo');
  br('bar');
});

gulp.task('default', ['browserify'])